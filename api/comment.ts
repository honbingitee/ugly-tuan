/*
 * @Author: hongbin
 * @Date: 2021-10-26 21:13:43
 * @Description: 评论接口
 * @FilePath: /uglyTuan/api/comment.ts
 */
import { request, query } from ".";
import { CommentType } from "./types";

interface ICreateComment {
  content: string;
  type: CommentType;
  sender: string;
}

export const createComment = (params: ICreateComment) =>
  request("/createComment", params);

export const queryComment = () => query("/queryComment");

interface IThumbsUp {
  _id: string;
}

export const thumbsUp = (params: IThumbsUp) => request("/thumbsUp", params);
