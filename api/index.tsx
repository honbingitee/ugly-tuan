/*
 * @Author: hongbin
 * @Date: 2021-10-26 21:19:32
 * @Description: 简单封装下axios
 * @FilePath: /uglyTuan/api/index.tsx
 */
import axios, { AxiosRequestConfig } from "axios";

axios.defaults.baseURL = "https://hongbin.xyz:7001";

// axios.interceptors.request.use((config: AxiosRdequestConfig) => {
//     if (typeof window !== "undefined") {
//       config.headers.Authorization = getLocalJWT();
//     }
//     return config;
//   });

// fetch("http://hongbin.xyz:7001/queryComment")
fetch("https://hongbin.xyz:7001/queryComment")
  .then(res => res.json())
  .then(res => {
    console.log("res", res);
  })
  .catch(err => {
    console.log("err", err);
  });

const { get, post } = axios;
export { get, post };

export const query: (
  url: string,
  config?: AxiosRequestConfig | undefined
) => Promise<any> = async (url, config) =>
  await get(url, config)
    .then(res => res)
    .catch(err => {
      console.error(url + " err", err, err.response);
      const message = err.response?.data?.message;
      if (message) console.log({ message });
      return { err: err.response };
    });

export const request: <T = any>(
  url: string,
  data?: any,
  config?: AxiosRequestConfig | undefined
) => Promise<{ data?: T; err?: Response }> = (...props) => {
  return new Promise(resolve => {
    post(...props)
      .then(res => resolve(res))
      .catch(err => {
        console.error("request" + props[0] + " err", err, err.response);
        const message = err.response?.data?.message;
        if (message) console.log({ message });
        resolve({ err: err.response });
      });
  });
};
