interface Sender {
  _id: string;
  nickname: string;
}

export enum CommentType {
  "text" = 0, //文本类型
  "image" = 1, //图片
  "text&image" = 3, //图文
}

export interface IComment {
  _id: string;
  content: string;
  thumbsUp: number;
  sender: [Sender];
  type: CommentType;
}
