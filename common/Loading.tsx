import React, { FC, memo, ReactElement, useEffect, useState } from "react";
import { Animated, Easing } from "react-native";
import { AntDesign } from "@expo/vector-icons";

const AnimateIcon = Animated.createAnimatedComponent(AntDesign);

const LoadingIcon: FC<Partial<React.ComponentProps<typeof AntDesign>>> = (
  props
): ReactElement => {
  const [rotate] = useState(new Animated.Value(0));

  const starRotate = () => {
    rotate.setValue(0);
    Animated.timing(rotate, {
      toValue: 1,
      duration: 1000,
      easing: Easing.linear,
      useNativeDriver: false,
    }).start(starRotate);
  };

  useEffect(() => {
    starRotate();
  }, []);

  return (
    <AnimateIcon
      name="loading1"
      size={24}
      color="black"
      style={[
        {
          transform: [
            {
              rotate: rotate.interpolate({
                inputRange: [0, 1],
                outputRange: ["0deg", "360deg"],
              }),
            },
          ],
        },
        props.style || {},
      ]}
      {...props}
    />
  );
};

export default memo(LoadingIcon);
