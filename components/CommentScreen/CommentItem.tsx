import React, { useState } from "react";
import { View as RView, StyleSheet } from "react-native";
import { IComment } from "../../api/types";
import { Text } from "../Themed";
// import { MaterialCommunityIcons } from "@expo/vector-icons";
// import Colors from "../../constants/Colors";
import useColorScheme from "../../hooks/useColorScheme";
import { thumbsUp } from "../../api/comment";
import { Button } from "react-native-paper";

interface props {
  comment: IComment;
}

const CommentItem: React.FC<props> = ({ comment }): React.ReactElement => {
  const colorScheme = useColorScheme();
  const [thumbs, setThumbs] = useState(comment.thumbsUp || 0);
  const [thumbsUpping, setThumbsUpping] = useState(false);

  const handleThumbsUp = async () => {
    setThumbsUpping(true);
    const { data } = await thumbsUp({ _id: comment._id });
    if (data.code == 0) {
      setThumbs(count => count + 1);
    }
    setThumbsUpping(false);
  };

  return (
    <RView
      style={{
        ...styles.wrap,
        backgroundColor: colorScheme === "light" ? "#f2f2f2fb" : "#333",
      }}
    >
      <RView
        style={{
          ...styles.commentItem,
          backgroundColor: colorScheme === "light" ? "#fff" : "#222",
        }}
      >
        <Text>{comment.content}</Text>
        <Text style={{ fontSize: 12, color: "#ccc" }}>
          {new Date().toLocaleDateString()}
        </Text>
      </RView>
      <RView
        style={{
          flexDirection: "row",
          alignItems: "center",
          paddingHorizontal: 2,
        }}
      >
        <Text style={{ flex: 1 }} colorName="tint">
          {comment.sender[0].nickname}
        </Text>
        {/* <MaterialCommunityIcons
          name="thumb-up-outline"
          size={20}
          color={Colors[colorScheme].text}
          onPress={handleThumbsUp}
        /> */}
        <Button
          onPress={!thumbsUpping ? handleThumbsUp : undefined}
          compact
          icon="thumb-up-outline"
          loading={thumbsUpping}
        >
          {thumbs}
        </Button>
      </RView>
    </RView>
  );
};

export default CommentItem;

const styles = StyleSheet.create({
  commentItem: {
    borderRadius: 2,
    elevation: 1,
    padding: 4,
    marginBottom: 5,
  },
  wrap: {
    marginVertical: 5,
    elevation: 3,
    borderRadius: 2,
    marginHorizontal: 10,
    paddingBottom: 5,
  },
});
