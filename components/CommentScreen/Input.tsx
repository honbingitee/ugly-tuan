import React, { useState, useEffect, memo } from "react";
import {
  Text,
  Animated,
  Keyboard,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import { createComment } from "../../api/comment";
import { IComment } from "../../api/types";
import Loading from "../../common/Loading";
import Colors from "../../constants/Colors";
import useColorScheme from "../../hooks/useColorScheme";

interface props {
  onSendComment: (comment: IComment) => void;
}

const Input: React.FC<props> = ({ onSendComment }) => {
  const colorScheme = useColorScheme();
  const [height] = useState(new Animated.Value(50));
  const [content, setContent] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
    Keyboard.addListener("keyboardDidHide", _keyboardDidHide);
    return () => {
      Keyboard.removeAllListeners("keyboardDidShow");
      Keyboard.removeAllListeners("keyboardDidHide");
    };
  }, []);

  const _keyboardDidShow = () => {
    Animated.timing(height, {
      duration: 500,
      toValue: 150,
      useNativeDriver: false,
    }).start();
  };

  const _keyboardDidHide = () => {
    Animated.timing(height, {
      duration: 300,
      toValue: 50,
      useNativeDriver: false,
    }).start();
  };

  const sendComment = () => {
    if (!content) alert("没有内容呀 啊 sir");
    setLoading(true);
    const params = {
      content,
      type: 0,
      sender: "617ba3a645c2dbfa30e87111",
    };
    createComment(params).then(({ data }) => {
      if (data.code === 0) {
        console.log(data);
        onSendComment(data.payload);
        setContent("");
        Keyboard.dismiss();
      } else alert("发送失败");
      setLoading(false);
    });
  };

  return (
    <Animated.View
      style={[
        { height, backgroundColor: Colors[colorScheme].background },
        styles.inputWrap,
      ]}
    >
      <TextInput
        multiline
        style={{
          backgroundColor: Colors[colorScheme].deep,
          ...styles.input,
        }}
        placeholder="评论"
        value={content}
        onChangeText={setContent}
      />
      <Animated.View
        style={{
          opacity: height.interpolate({
            inputRange: [50, 150],
            outputRange: [0, 1],
          }),
          backgroundColor: Colors[colorScheme].primary,
          ...styles.sendBtn,
        }}
      >
        <TouchableOpacity style={styles.touchBtn} onPress={sendComment}>
          {loading ? <Loading color="#fff" size={20} /> : null}
          <Text style={styles.sendText}>发送</Text>
        </TouchableOpacity>
      </Animated.View>
    </Animated.View>
  );
};

export default memo(Input);

const styles = StyleSheet.create({
  inputWrap: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    borderRadius: 3,
    width: "96%",
    height: "90%",
    padding: 10,
    paddingTop: 12,
    textAlignVertical: "top",
  },
  sendBtn: {
    position: "absolute",
    bottom: "8%",
    right: "4%",
    elevation: 4,
    borderRadius: 5,
    width: 100,
    height: 30,
  },
  sendText: { color: "#FFF", marginLeft: 5 },
  touchBtn: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    flex: 1,
  },
});
