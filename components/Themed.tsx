/**
 * Learn more about Light and Dark modes:
 * https://docs.expo.io/guides/color-schemes/
 */

import * as React from "react";
import { Text as DefaultText, View as DefaultView } from "react-native";
import { StatusBar } from "expo-status-bar";

import Colors from "../constants/Colors";
import useColorScheme from "../hooks/useColorScheme";
import { IOS } from "../constants/Layout";

type TColorName = keyof typeof Colors.light & keyof typeof Colors.dark;

export function useThemeColor(
  props: { light?: string; dark?: string },
  colorName: TColorName
) {
  const theme = useColorScheme();
  const colorFromProps = props[theme];

  if (colorFromProps) {
    return colorFromProps;
  } else {
    return Colors[theme][colorName];
  }
}

type ThemeProps = {
  lightColor?: string;
  darkColor?: string;
  colorName?: TColorName;
  themeName?: TColorName;
  themeType?: "backgroundColor" | "borderColor";
};

export type TextProps = ThemeProps & DefaultText["props"];
export type ViewProps = ThemeProps & DefaultView["props"];

export function Text(props: TextProps) {
  const {
    style,
    lightColor,
    darkColor,
    themeName = "background",
    colorName = "text",
    themeType,
    ...otherProps
  } = props;
  const color = useThemeColor(
    { light: lightColor, dark: darkColor },
    colorName
  );
  const themeStyle: any = { color };

  if (themeType) {
    themeStyle[themeType] = useThemeColor(
      { light: lightColor, dark: darkColor },
      themeName
    );
  }

  return <DefaultText style={[themeStyle, style]} {...otherProps} />;
}

export function View(props: ViewProps) {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const backgroundColor = useThemeColor(
    { light: lightColor, dark: darkColor },
    "background"
  );

  return <DefaultView style={[{ backgroundColor }, style]} {...otherProps} />;
}

export function Statusbar() {
  // /* Use a light status bar on iOS to account for the black space above the modal */
  return <StatusBar style={IOS ? "light" : "auto"} />;
}
