import React, { useCallback, useEffect, useState } from "react";
import { StyleSheet, ScrollView, KeyboardAvoidingView } from "react-native";
import { queryComment } from "../api/comment";
import { IComment } from "../api/types";
import CommentItem from "../components/CommentScreen/CommentItem";
import Input from "../components/CommentScreen/Input";

export default function CommentScreen() {
  const [comments, setComments] = useState<IComment[]>([]);

  useEffect(() => {
    queryComment().then(({ data }) => {
      console.log(data);
      if (data) {
        setComments(data.comments);
      }
    });
  }, []);

  const handleSendComment = useCallback((comment: IComment) => {
    setComments(prev => [comment, ...prev]);
  }, []);

  return (
    <KeyboardAvoidingView style={styles.container}>
      <ScrollView style={styles.scrollView}>
        {comments.map(comment => (
          <CommentItem key={comment._id} comment={comment} />
        ))}
      </ScrollView>
      <Input onSendComment={handleSendComment} />
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  scrollView: {
    flex: 1,
    width: "100%",
  },
});

// import { RootTabScreenProps } from "../types";

// export default function CommentScreen({
//   navigation,
// }: RootTabScreenProps<"Comment">) {
