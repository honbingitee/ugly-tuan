import React from "react";
import { StyleSheet } from "react-native";
import { View } from "../components/Themed";
import { Button, Snackbar } from "react-native-paper";

export default function TabTwoScreen() {
  const [visible, setVisible] = React.useState(false);

  return (
    <View style={styles.container}>
      <Button
        icon="hand-right"
        mode="contained"
        onPress={() => {
          setVisible(true);
        }}
      >
        登陆
      </Button>
      <Snackbar
        visible={visible}
        onDismiss={() => setVisible(false)}
        action={{
          label: "已阅",
          onPress: () => {
            // Do something
          },
        }}
      >
        正在开发中。。。
      </Snackbar>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
